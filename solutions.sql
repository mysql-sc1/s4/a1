-- FIND ALL ARTISTS THAT HAS THE LETTER 'D' IN THEIR NAME
SELECT * FROM artists WHERE name LIKE "%d%";

-- FIND ALL SONGS THAT HAS A LENGTH OF LESS THAN 230
SELECT * FROM songs WHERE length < 230;

-- JOIN THE 'ALBUMS' AND 'SONGS' TABLES. (ONLY SHOW THE ALBUM NAME, SONG NAME, AND SONG LENGTH)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- JOIN 'ARTISTS' AND 'ALBUMS' TABLES (FIND ALL ALBUMS THAT HAS THE LETTER A IN ITS NAME)
SELECT * FROM artists 
    JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- SORT THE ALBUMs IN Z-A ORDER (SHOW ONLY THE FIRST 4 RECORDS)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- JOIN THE 'ALBUMS' AND 'SONGS' TABLES (SORT ALBUMS FROM Z-A AND SORT SONGS FROM A-Z)
SELECT * FROM albums 
    JOIN songs ON albums.id = songs.album_id ORDER BY song_name ASC, album_title DESC;
